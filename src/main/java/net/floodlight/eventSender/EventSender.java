package net.floodlight.eventSender;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EventSender extends Thread {
	private String src_mac;
	private String dst_mac;
	private String srcp_ip;
	private String dst_ip;
	private String src_port;
	private String dst_port;
	private String in_port;
	private String ethtype;
	private String network_protocol;
	private int connection_port;
	private String connection_ip="10.0.0.9";
	private String info_to_send;
	private String message_type;

	public void run(){
		try{
		Socket conn=new Socket(this.connection_ip,this.connection_port);
		PrintWriter out= new PrintWriter(conn.getOutputStream());
		out.write(this.info_to_send);
		out.flush();
		conn.close();
		System.out.println("Evento "+ this.message_type+" enviado");
	}catch (IOException ioException){
		System.out.println("Problema ocurrido para el evento : "+ this.message_type);
		System.out.println(ioException);
	}

	}
	public void setValues(String src_mac,String dst_mac,String src_ip,String dst_ip,String src_port,String dst_port,String ethtype,String network_protocol, String in_port){
		this.src_mac=src_mac;
		this.dst_mac=dst_mac;
		this.srcp_ip=src_ip;
		this.dst_ip=dst_ip;
		this.src_port=src_port;
		this.dst_port=dst_port;
		this.in_port=in_port;
		this.ethtype=ethtype;
		this.network_protocol=network_protocol;
		
	}
	public void setMessageType(String message_type,String message){
		this.message_type=message_type;
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date=new Date();
		if(message_type=="TenantDetecter"){
			this.connection_port=60000;
			this.info_to_send= "{\"date\":\""+dateFormat.format(date)+"\",\"message_type\":\"tenantDetecter\",\"message\":\""+message+"\",\"src mac\":\""+this.src_mac +"\",\"ip\":\""+this.srcp_ip+"\"}";

		}else if(message_type=="Communications"){
			this.connection_port=60002;
			this.info_to_send="{\"date\":\""+dateFormat.format(date)+"\",\"message_type\":\"communication detected\",\"message\":\""+message+"\",\"src mac\":\""+this.src_mac +"\",\"dst mac\":\""+this.dst_mac+"\",\"src ip\":\""+this.srcp_ip +"\",\"dst ip\":\""+this.dst_ip+"\",\"src port\":\""+this.src_port+"\",\"dst port\":\""+this.dst_port+"\",\"eth type\":\""+this.ethtype+"\",\"protocol\":\""+this.network_protocol+"\",\"inport\":\""+this.in_port+"\"}";
		}
	}
	
}
