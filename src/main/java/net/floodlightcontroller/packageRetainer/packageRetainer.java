package net.floodlightcontroller.packageRetainer;

import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.counter.ICounterStoreService;
import net.floodlightcontroller.learningswitch.LearningSwitch;

import org.openflow.protocol.OFFlowMod;
import org.openflow.protocol.OFMatch;
import org.openflow.protocol.OFPacketIn;
import org.openflow.protocol.OFPacketOut;

public class packageRetainer extends Thread{
	IOFSwitch sw;
	OFMatch match;
	OFPacketIn pi;
	short outport;
	IFloodlightProviderService floodlightProvider;
	ICounterStoreService counterStore;
	LearningSwitch Switch;
	public packageRetainer(IOFSwitch sw, OFMatch match, OFPacketIn pi, short outport,IFloodlightProviderService floodlightProvider,ICounterStoreService counterStore,LearningSwitch Switch){
		this.sw=sw;
		this.match=match;
		this.pi=pi;
		this.outport=outport;
		this.floodlightProvider=floodlightProvider;
		this.counterStore=counterStore;
		this.Switch=Switch;
	}
	public void run(){
		try {
			Thread.sleep(1);
			//LearningSwitch learning=new LearningSwitch();
			//learning.setFloodlightProvider(this.floodlightProvider);
			//learning.setCounterStore(this.counterStore);
			this.Switch.pushPacket(this.sw, this.match, this.pi, this.outport);
			this.Switch.writeFlowMod(this.sw, OFFlowMod.OFPFC_ADD, OFPacketOut.BUFFER_ID_NONE,  this.match, this.outport);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
