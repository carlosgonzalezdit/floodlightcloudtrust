package net.floodlightcontroller.tenantdetect;
import net.floodlight.eventSender.EventSender;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.devicemanager.IDevice;
import net.floodlightcontroller.devicemanager.IDeviceListener;
import net.floodlightcontroller.devicemanager.IDeviceService;
import net.floodlightcontroller.devicemanager.IEntityClass;
import net.floodlightcontroller.devicemanager.SwitchPort;
import net.floodlightcontroller.packet.IPv4;

public class TenantDetecter implements IDevice, IDeviceListener,
		IFloodlightModule {
	protected IDeviceService device_manager;
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "TenantDetecter";
	}

	@Override
	public boolean isCallbackOrderingPrereq(String type, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCallbackOrderingPostreq(String type, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleServices() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Class<? extends IFloodlightService>, IFloodlightService> getServiceImpls() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
		// TODO Auto-generated method stub
		Collection<Class<? extends IFloodlightService>> l =new ArrayList<Class<? extends IFloodlightService>>();
		l.add(IDeviceService.class);
		return l;	}

	@Override
	public void init(FloodlightModuleContext context)
			throws FloodlightModuleException {
        device_manager = context.getServiceImpl(IDeviceService.class);

		// TODO Auto-generated method stub

	}

	@Override
	public void startUp(FloodlightModuleContext context)
			throws FloodlightModuleException {
		// TODO Auto-generated method stub
		device_manager.addListener(this);

	}

	@Override
	public void deviceAdded(IDevice device) {
		// TODO Auto-generated method stub
		System.out.println("Dispositivo Detectado");
		System.out.println(device.getMACAddressString());
		System.out.println(device.getIPv4Addresses().length);
		if(device.getIPv4Addresses().length>0){
			System.out.println(IPv4.fromIPv4Address(device.getIPv4Addresses()[0]));
		}
		String ip="No descubierta";
		if(device.getIPv4Addresses().length>0){
			ip=IPv4.fromIPv4Address(device.getIPv4Addresses()[0]);
		}
		EventSender sender=new EventSender();
		sender.setValues(device.getMACAddressString(),"NO",ip,"NO","NO","NO","NO","NO","NO");
		sender.setMessageType("TenantDetecter","New Device");
		sender.start();
		//this.sendLogMessage(device.getMACAddressString(),ip,"New Device");
		

	}
	public void sendLogMessage(String mac_address,String ip,String message){
		try{

		Socket conn=new Socket("10.0.0.9",60000);
		PrintWriter out= new PrintWriter(conn.getOutputStream());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date=new Date();
		String json = "{\"date\":\""+dateFormat.format(date)+"\",\"message_type\":\"tenantDetecter\",\"message\":\""+message+"\",\"src mac\":\""+mac_address +"\",\"ip\":\""+ip+"\"}";
		out.write(json);
		out.flush();
		conn.close();
	}catch (IOException ioException){
		System.out.println("Problema ocurrido");
		System.out.println(ioException);
	}

	}

	@Override
	public void deviceRemoved(IDevice device) {
		System.out.println("Dispositivo eliminado");
		System.out.print(device.getMACAddressString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date=new Date();
		System.out.println(dateFormat.format(date));
	}

	@Override
	public void deviceMoved(IDevice device) {
		// TODO Auto-generated method stub
	    System.out.println("Dispositivo movido");
		System.out.println(device.getMACAddressString());
		if(device.getIPv4Addresses().length>0){
		System.out.println(IPv4.fromIPv4Address(device.getIPv4Addresses()[0]));
		}

	}

	@Override
	public void deviceIPV4AddrChanged(IDevice device) {
		// TODO Auto-generated method stub
		System.out.println("Dispositivo ha cambiado de IP");
		System.out.println(device.getMACAddressString());
		System.out.println(IPv4.fromIPv4Address(device.getIPv4Addresses()[0]));
		String ip="No descubierta";
		if(device.getIPv4Addresses().length>0){
			ip=IPv4.fromIPv4Address(device.getIPv4Addresses()[0]);
		}
		EventSender sender=new EventSender();
		sender.setValues(device.getMACAddressString(),"NO",ip,"NO","NO","NO","NO","NO","NO");
		sender.setMessageType("TenantDetecter","Device Changed IP");
		sender.start();
		//this.sendLogMessage(device.getMACAddressString(),ip,"Device Changed IP");


	}
	

	@Override
	public void deviceVlanChanged(IDevice device) {
		// TODO Auto-generated method stub

	}

	@Override
	public Long getDeviceKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getMACAddress() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getMACAddressString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Short[] getVlanId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer[] getIPv4Addresses() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SwitchPort[] getAttachmentPoints() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SwitchPort[] getOldAP() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SwitchPort[] getAttachmentPoints(boolean includeError) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Short[] getSwitchPortVlanIds(SwitchPort swp) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getLastSeen() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IEntityClass getEntityClass() {
		// TODO Auto-generated method stub
		return null;
	}

}
