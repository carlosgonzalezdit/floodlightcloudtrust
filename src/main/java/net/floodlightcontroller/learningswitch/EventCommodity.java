package net.floodlightcontroller.learningswitch;

import org.restlet.resource.ServerResource;
import org.restlet.resource.Post;
import org.restlet.resource.Get;
import org.restlet.resource.Delete;
public class EventCommodity extends ServerResource{
@Post("json")
public void addMacNoEvent(){
    String mac = (String) getRequestAttributes().get("mac");
	 ILearningSwitchService lsp =
             (ILearningSwitchService)getContext().getAttributes().
                 get(ILearningSwitchService.class.getCanonicalName());
	 lsp.addNoEventSender(mac);
}
}
