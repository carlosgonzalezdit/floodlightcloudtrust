package net.floodlightcontroller.learningswitch;

import org.restlet.resource.ServerResource;
import org.restlet.resource.Post;
import org.restlet.resource.Get;
import org.restlet.resource.Delete;

public class SwitchCommodity  extends ServerResource{
@Post("json")
public void addMacToSwitch(){
    String mac = (String) getRequestAttributes().get("mac");
	 ILearningSwitchService lsp =
             (ILearningSwitchService)getContext().getAttributes().
                 get(ILearningSwitchService.class.getCanonicalName());
	 lsp.addDeviceToSwitchService(mac);
}

@Get("json")
public void deleteMac(){
	String mac = (String) getRequestAttributes().get("mac");
	 ILearningSwitchService lsp =
            (ILearningSwitchService)getContext().getAttributes().
                get(ILearningSwitchService.class.getCanonicalName());
	 lsp.deleteDeviceFromSwitchService(mac);
}
}
