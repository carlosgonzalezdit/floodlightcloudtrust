package net.floodlightcontroller.learningswitch;
import net.floodlightcontroller.core.IOFSwitch;

import org.openflow.protocol.OFMatch;
import org.openflow.protocol.OFPacketIn;
import org.restlet.resource.ServerResource;

public class CommoditySwitchRules {
	public IOFSwitch sw;
	public OFPacketIn pkt;
	public OFMatch match;
	public String sourceMac;
	public String destMac;
	public short outPutPort;
	public CommoditySwitchRules(IOFSwitch sw, OFMatch match, String sourceMac,String destMac,short outPutPort,OFPacketIn pkt){
		this.sw=sw;
		this.match=match;
		this.sourceMac=sourceMac;
		this.destMac=destMac;
		this.outPutPort=outPutPort;
		this.pkt=pkt;
	}
}
